import { Injectable } from "@angular/core";
import * as d3 from "d3";

const dateFormat = d3.timeFormat("%Y-%m-%d");

@Injectable({
  providedIn: "root",
})
export class CalendarService {
  data([dateStart, dateStop]: any): any {
    return d3.timeDay
      .range(dateStart, dateStop, 1)
      .map((date, i) => ({
        date,
        values: Array.from(
          Array(d3.timeDay.count(d3.timeYear.floor(date), date) % 10)
        ).map((_, j) => ({ id: 0, text: `Item ${i}${j}` })),
      }))
      .reduce((acc, o) => ({ ...acc, [dateFormat(o.date)]: o }), {});
  }

  constructor() {}
}
