import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { pluck } from "rxjs/operators";

@Component({
  selector: "app-week-page",
  template: `<app-calendar-week [week]="week$ | async"></app-calendar-week>`,
  styles: [],
})
export class WeekPageComponent implements OnInit {
  week$ = this.route.params.pipe(pluck("week"));

  constructor(public route: ActivatedRoute) {}

  ngOnInit(): void {}
}
