import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MaterialModule } from "./material.module";
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarPageComponent } from './calendar-page/calendar-page.component';
import { WeekPageComponent } from './week-page/week-page.component';
import { CalendarWeekComponent } from './calendar-week/calendar-week.component';
import { CalendarItemDialogComponent } from './calendar-item-dialog/calendar-item-dialog.component';

@NgModule({
  declarations: [AppComponent, CalendarComponent, CalendarPageComponent, WeekPageComponent, CalendarWeekComponent, CalendarItemDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
