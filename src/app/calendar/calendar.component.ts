import {
  Input,
  SimpleChanges,
  OnChanges,
  Component,
  OnInit,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { transition, trigger, animate, style } from "@angular/animations";
import * as d3 from "d3";

import { CalendarItemDialogComponent } from "../calendar-item-dialog/calendar-item-dialog.component";
import { CalendarService } from "../calendar.service";

export enum CalendarView {
  Month = "month",
  Week = "week",
  Day = "day",
}

const cmpWeek = ([a0, a1], [b0, b1]) =>
  a0 < b0 ? -1 : a0 > b0 ? 1 : a1 < b1 ? -1 : a1 > b1 ? 1 : 0;

@Component({
  animations: [
    trigger("fadeIn", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate("0.1s", style({ opacity: 1 })),
      ]),
      transition(":leave", [
        style({ opacity: 1 }),
        animate("0.1s", style({ opacity: 0 })),
      ]),
    ]),
    trigger("slide", [
      transition(
        (fromState: any, toState: any, element, params) => {
          console.log(fromState);
          if (fromState == null || toState == null) {
            return false;
          }
          return cmpWeek(toState, fromState) == -1;
        },
        [
          style({ opacity: 1, transform: "translate(0,0)" }),
          animate(
            "0.5s",
            style({ opacity: 0, transform: "translate(-10%,0)" })
          ),
        ]
      ),
      transition(
        (fromState: any, toState: any, element, params) => {
          if (fromState == null || toState == null) {
            return false;
          }
          return cmpWeek(toState, fromState) == 1;
        },
        [
          style({ opacity: 1, transform: "translate(0,0)" }),
          animate("0.5s", style({ opacity: 0, transform: "translate(10%,0)" })),
        ]
      ),
    ]),
  ],
  selector: "app-calendar",
  template: `
    <div class="header" [ngClass]="view">
      <div *ngFor="let o of headerDays">
        {{ formatDay[view](o.date) }}
      </div>
    </div>
    <ng-container [ngSwitch]="view">
      <ng-container *ngSwitchCase="'day'">
        <div class="days day" @fadeIn>
          <div>
            <div class="items">
              <div
                *ngFor="let value of getDataForDate(date)?.values"
                (click)="preview(value.id, $event)"
              >
                {{ value.text }}
              </div>
            </div>
          </div>
        </div>
      </ng-container>
      <ng-container *ngSwitchCase="'week'">
        <div class="days" @fadeIn>
          <ng-container *ngFor="let o of weekDays">
            <div
              class="day"
              [routerLink]="['/calendar']"
              [queryParams]="{ date: dateFormat(o.date), view: 'day' }"
            >
              <div class="items">
                <div
                  *ngFor="let value of getDataForDate(o.date)?.values"
                  (click)="preview(value.id, $event)"
                >
                  {{ value.text }}
                </div>
              </div>
            </div>
          </ng-container>
        </div>
      </ng-container>
      <ng-container *ngSwitchDefault>
        <div class="days month" @fadeIn>
          <div
            *ngFor="let o of monthDays"
            [class.today]="+o.date == +today"
            [class.active]="+date == +o.date"
            [class.inactive]="o.month !== activeMonth"
            [routerLink]="['/calendar']"
            [queryParams]="{ date: dateFormat(o.date), view: 'week' }"
          >
            <span>{{
              (o.month === activeMonth ? format : formatWithMonth)(o.date)
            }}</span>
            <div class="items">
              <div
                *ngFor="let value of getDataForDate(o.date)?.values"
                (click)="preview(value.id, $event)"
              >
                {{ value.text }}
              </div>
            </div>
          </div>
        </div>
      </ng-container>
    </ng-container>
  `,
  styleUrls: ["./calendar.component.scss"],
})
export class CalendarComponent implements OnInit, OnChanges {
  @Input()
  view = CalendarView.Month;

  @Input()
  date?: Date = d3.timeDay.floor(new Date());

  get headerDays() {
    switch (this.view) {
      case "month":
        return this.monthDays.slice(0, 7);
      case "week":
        return this.weekDays;
      case "day":
        return [{ date: this.date }];
      default:
        return [];
    }
  }

  today = d3.timeDay.floor(new Date());
  week?: [number, number];

  activeMonth: number;
  monthDays: { date: Date; i: number; j: number; month: number }[];
  weekDays: { date: Date }[];

  dateFormat = d3.timeFormat("%Y-%m-%d");

  format = d3.timeFormat("%_d");
  formatWithMonth = d3.timeFormat("%b %_d");
  formatDay = {
    month: d3.timeFormat("%a"),
    week: d3.timeFormat("%a, %b %_d"),
    day: d3.timeFormat("%a, %b %_d %Y"),
  };

  data?: { [date: string]: { date: Date; values: any[] } };

  constructor(public service: CalendarService, public dialog: MatDialog) {}

  ngOnInit(): void {}

  preview(id, event) {
    const dialogRef = this.dialog.open(CalendarItemDialogComponent, {
      width: "250px",
      data: { id },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed");
    });
    event.preventDefault();
    event.stopPropagation();
    return false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ("date" in changes) {
      const month = d3.timeMonth.floor(this.date);
      this.activeMonth = month.getMonth();

      const monthDays = d3.timeDay
        .range(
          d3.timeWeek.floor(month),
          d3.timeWeek.ceil(d3.timeMonth.ceil(d3.timeMinute.offset(this.date))),
          1
        )
        .map((date, index) => ({
          month: date.getMonth(),
          i: index % 7,
          j: Math.floor(index / 7),
          date,
        }));
      this.monthDays = monthDays;

      const weekDays = d3.timeDay
        .range(
          d3.timeWeek.floor(this.date),
          d3.timeWeek.ceil(d3.timeMinute.offset(this.date)),
          1
        )
        .map((date, index) => ({ date, index }));
      this.week = [
        this.date.getFullYear(),
        d3.timeWeek.count(d3.timeYear(this.date), this.date),
      ];
      this.weekDays = weekDays;

      const dateRange =
        this.view == "month"
          ? [monthDays[0].date, monthDays[monthDays.length - 1].date]
          : this.view == "week"
          ? [weekDays[0].date, weekDays[weekDays.length - 1].date]
          : ([this.date, d3.timeDay.offset(this.date, 1)] as [Date, Date]);
      this.data = this.service.data(dateRange);
    }
  }

  getDataForDate(date: Date) {
    return this.data[this.dateFormat(date)];
  }
}
