import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CalendarPageComponent } from "./calendar-page/calendar-page.component";
import { WeekPageComponent } from "./week-page/week-page.component";

const routes: Routes = [
  { path: "", component: CalendarPageComponent },
  { path: "calendar", component: CalendarPageComponent },
  { path: "week/:week", component: WeekPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
