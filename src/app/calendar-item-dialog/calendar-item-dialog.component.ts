import { Inject, Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-calendar-item-dialog',
  template: `
    <p>Item {{ id }}</p>
  `,
  styles: [
  ]
})
export class CalendarItemDialogComponent implements OnInit {
  id?: any;

  constructor( public dialogRef: MatDialogRef<CalendarItemDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.id = data.id;
  }

  ngOnInit(): void {
  }

}
