import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MatDatepicker } from "@angular/material/datepicker";
import {
  first,
  shareReplay,
  map,
  pluck,
  distinctUntilChanged,
} from "rxjs/operators";
import * as d3 from "d3";

const dateFormat = d3.timeFormat("%Y-%m-%d");
const dateParse = d3.timeParse("%Y-%m-%d");
const monthFormat = d3.timeFormat("%B");

@Component({
  selector: "app-calendar-page",
  template: `
    <ng-container *ngIf="params$ | async as params">
      <div class="header">
        <span></span>
        <div class="controls">
          <div>
            <button mat-icon-button (click)="decrement()">
              <mat-icon>chevron_left</mat-icon>
            </button>
          </div>
          <div class="title">
            <button mat-button (click)="picker.open()">
              {{ params.monthName }} {{ year }}
            </button>
            <mat-datepicker startView="year" #picker> </mat-datepicker>
            <input
              matInput
              [matDatepicker]="picker"
              [value]="params.date"
              (dateChange)="handler($event.value)"
            />
          </div>
          <div>
            <button mat-icon-button (click)="increment()">
              <mat-icon>chevron_right</mat-icon>
            </button>
          </div>
        </div>
        <div>
          <ng-container *ngIf="view$ | async as view">
            <button mat-stroked-button [matMenuTriggerFor]="menu">
              {{ view | titlecase }}
            </button>
            <mat-menu #menu="matMenu">
              <button
                *ngFor="let v of filterViews(views, view)"
                (click)="changeView(v)"
                mat-menu-item
              >
                {{ v | titlecase }}
              </button>
            </mat-menu>
          </ng-container>
          <button mat-icon-button (click)="sidenav.toggle()"><mat-icon>settings</mat-icon></button>
        </div>
      </div>
      <mat-sidenav-container>
        <mat-sidenav #sidenav mode="side" position="end">
          <mat-selection-list #shoes>
            <mat-list-option *ngFor="let each of ['one', 'two', 'three']">
              {{each}}
            </mat-list-option>
          </mat-selection-list>
        </mat-sidenav>
        <mat-sidenav-content>
          <app-calendar [date]="params.date" [view]="params.view"></app-calendar>
        </mat-sidenav-content>
      </mat-sidenav-container>
    </ng-container>
  `,
  styleUrls: ["./calendar-page.component.scss"],
})
export class CalendarPageComponent {
  params$ = this.route.queryParams.pipe(
    map(({ date, view }) => ({
      date: dateParse(date) || new Date(),
      view: view || "month",
    })),
    distinctUntilChanged((p0, p1) => p0.view == p1.view && p0.date == p1.date),
    map((params) => ({ ...params, monthName: monthFormat(params.date) })),
    shareReplay(1)
  );

  view$ = this.params$.pipe(pluck("view"), distinctUntilChanged());
  views = ["day", "week", "month"];

  filterViews(a, v) {
    return a.filter((_v) => v !== _v);
  }

  ngAfterViewInit(): void {}

  increment() {
    this.offset(1);
  }
  decrement() {
    this.offset(-1);
  }

  private offset(dir: number) {
    this.params$.pipe(first()).subscribe((v) => {
      const { date: lastDate, view } = v;
      let date;
      switch (view) {
        case "month": {
          date = d3.timeMonth.floor(d3.timeMonth.offset(lastDate, dir));
          break;
        }
        case "week": {
          date = d3.timeWeek.floor(d3.timeWeek.offset(lastDate, dir));
          break;
        }
        case "day": {
          date = d3.timeDay.offset(lastDate, dir);
          break;
        }
        default: {
          // pass
        }
      }
      this.router.navigate(["/calendar"], {
        queryParams: { date: dateFormat(date) },
        queryParamsHandling: "merge",
      });
    });
  }

  handler(date: Date) {
    this.router.navigate(["/calendar"], {
      queryParams: { date: dateFormat(date) },
      queryParamsHandling: "merge",
    });
    //datepicker.close();
  }

  changeView(view) {
    this.router.navigate(["/calendar"], {
      queryParams: { view },
      queryParamsHandling: "merge",
    });
  }

  constructor(public route: ActivatedRoute, public router: Router) {}
}
