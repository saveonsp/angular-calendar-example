import {
  Input,
  OnChanges,
  SimpleChanges,
  Component,
  OnInit,
} from "@angular/core";
import * as d3 from "d3";

@Component({
  selector: "app-calendar-week",
  template: `
    <ng-container *ngFor="let day of weekDays">
      <div class="header">{{ formatDay(day) }}</div>
      <div class="day"></div>
    </ng-container>
  `,
  styleUrls: ["./calendar-week.component.scss"],
})
export class CalendarWeekComponent implements OnInit, OnChanges {
  @Input()
  week?: string;

  formatDay = d3.timeFormat("%a, %b %_d");
  getLink = (d: Date) => ["/week", d3.timeFormat("%Y-%V")(d)];

  get weekDays(): any[] {
    if (!this.week) {
      return [];
    }
    const parse = d3.timeParse("%Y-%V");
    const date = parse(this.week);
    return d3.timeDay.range(d3.timeWeek.floor(date), d3.timeWeek.ceil(date), 1);
  }

  constructor() {}

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {}
}
