# Setup  
0. Install nodejs
1. `npm install`
2. `npm start`

# File Description
`material.module.ts`  
contains useful elements / styles (buttons, dropdowns, lists)  

`app-routing.module.ts`  
maps routes to pages  
